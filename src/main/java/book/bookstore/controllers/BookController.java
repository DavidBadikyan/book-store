package book.bookstore.controllers;

import book.bookstore.data.model.Book;
import book.bookstore.data.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/")
public class BookController {

    @Autowired
    private BookService bookService;

    @PostMapping()
    public ResponseEntity<Book> create(@RequestBody Book book) {
        return ResponseEntity.ok(bookService.create(book));
    }

    @GetMapping("/books")
    public ResponseEntity<List<Book>> all() {
        return ResponseEntity.ok(bookService.getAll());
    }

    @GetMapping("/books/{id}")
    public ResponseEntity<Book> getById(@PathVariable("id") Long bookId) {
        return ResponseEntity.ok(bookService.getOne(bookId));
    }

    @PutMapping("/books/")
    public ResponseEntity<Book> update(
            @RequestBody Book book
    ) {
        return ResponseEntity.ok(bookService.update(book));
    }

    @DeleteMapping("/books/{id}")
    public boolean delete(@PathVariable("id") long bookId) {
        bookService.delete(bookId);
        return true;
    }

}
