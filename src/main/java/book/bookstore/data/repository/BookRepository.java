package book.bookstore.data.repository;

import book.bookstore.data.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends JpaRepository<Book, Long>, AbstractRepository<Book, Long> {

}
