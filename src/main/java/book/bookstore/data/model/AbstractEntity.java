package book.bookstore.data.model;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class AbstractEntity<T> {
    public abstract T getId();

    public abstract void setId(T id);
}
