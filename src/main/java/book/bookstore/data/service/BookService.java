package book.bookstore.data.service;

import book.bookstore.data.model.Book;
import book.bookstore.data.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    public Book create(Book book) {
        return save(book);
    }

    public List<Book> getAll() {
        return bookRepository.findAll();
    }

    public Book getOne(Long id) {
        return bookRepository.getOne(id);
    }

    public Book update(Book book) {
        Book currentBook = getOne(book.getId());
        if (currentBook != null) {
            if (book.getAuthor() != null) {
                currentBook.setAuthor(book.getAuthor());
            }
            if (book.getTitle() != null) {
                currentBook.setTitle(book.getTitle());
            }
            return save(currentBook);
        }
        return save(book);
    }

    public void delete(Long id) {
        bookRepository.delete(id);
    }

    private Book save(Book book) {
        return bookRepository.save(book);
    }
}
